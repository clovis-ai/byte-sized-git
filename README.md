# Byte-sized Git

Exercises to learn Git.
This repository is a companion to my [YouTube classes on Git](https://www.youtube.com/playlist?list=PLRltSu5QZhp5mn0Z8xlaUWgfXzcmagFgW).

## Table of contents

- [Creating commits](creating-commits.md)
